# syntax=docker/dockerfile:1
FROM python:3.7-alpine
WORKDIR /code
# variables d'environnements pour flask
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
# installation des dependances (flask et db redis)
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
# exposition du port 5000
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
